<?
$data = '{
    "transactionId": 1
}';
$data_string = json_encode($data);

$url = file_get_contents("../addr")."/ocppj/remotestarttransaction:5000";
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($data_string))
);

$result = curl_exec($ch);
curl_close($ch);

echo $result;