<?php
/**
 * Created by PhpStorm.
 * User: natalachilikina
 * Date: 25.02.16
 * Time: 11:20
 */
?>
<div class="navbar-nav navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.php"><img src="images/logo30.png" alt=""> SmartEVSE Dashboard</a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="/index.php"><i class="icon-home icon-white"></i> Home</a></li>
				<li><a href="tables.php"><i class="icon-th icon-white"></i> Tables</a></li>
				<li><a href="login.php"><i class="icon-lock icon-white"></i> Login</a></li>
				<li><a href="user.php"><i class="icon-user icon-white"></i> User</a></li>

			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div>
