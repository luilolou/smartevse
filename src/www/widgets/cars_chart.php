<!-- DONUT CHART BLOCK -->
<div class="col-sm-3 col-lg-3">
	<div class="dash-unit">
		<dtitle>Site Bandwidth</dtitle>
		<hr>
		<div id="load"></div>
		<h2>45%</h2>
	</div>
</div>

<!-- DONUT CHART BLOCK -->
<div class="col-sm-3 col-lg-3">
	<div class="dash-unit">
		<dtitle>Disk Space</dtitle>
		<hr>
		<div id="space"></div>
		<h2>65%</h2>
	</div>
</div>