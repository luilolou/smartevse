<div class="col-sm-3 col-lg-3">
	<div class="dash-unit">
		<dtitle>User Profile</dtitle>
		<hr>
		<div class="thumbnail">
			<img src="images/face80x80.jpg" alt="Marcel Newman" class="img-circle">
		</div><!-- /thumbnail -->
		<h1>Marcel Newman</h1>
		<h3>Madrid, Spain</h3>
		<br>
		<div class="info-user">
			<span aria-hidden="true" class="li_user fs1"></span>
			<span aria-hidden="true" class="li_settings fs1"></span>
			<span aria-hidden="true" class="li_mail fs1"></span>
			<span aria-hidden="true" class="li_key fs1"></span>
		</div>
	</div>
</div>