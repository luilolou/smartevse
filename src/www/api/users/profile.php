<?php
/**
 * Created by PhpStorm.
 * User: Natalia Chilikina
 * Date: 24.02.16
 * Time: 13:46
 */

echo '
{
    "id": "1",
    "name": "Bertran Meyer",
    "login": "meyer",
    "cars": [{"id": "2", "model": "peugeot", "year": "2014"}, {"id": "3", "model": "bmw", "year": "1974"}],
    "email": "b.meyer@innopolis.ru",
    "image": "http://cacm.acm.org/system/assets/0001/5470/041514_Bertrand-Meyer-250.large.jpg?1397569694&1397569694"
}
';